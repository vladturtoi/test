<?php
/**
 * Created by PhpStorm.
 * User: vlad.turtoi
 * Date: 10/10/2017
 * Time: 11:07
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class AuthController extends Controller
{
	const RULES_AUTH = [
		'username' => 'required',
		'password' => 'required'
	];

	public function login(Request $request)
	{
		$this->validate($request, self::RULES_AUTH);

		$response = [];
		$response['token'] = base64_encode(
			$request->username
				. ' '
				. encrypt($request->password)
		);

		return $response;
	}
}