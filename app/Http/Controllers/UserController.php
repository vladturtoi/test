<?php
/**
 * Created by PhpStorm.
 * User: vlad.turtoi
 * Date: 10/10/2017
 * Time: 11:33
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
	const RULES_UPDATE_REQUEST = [
		'token' => 'required|string',
		'email' => 'required|email',
		'birthdate' => 'required|date|before:10 years ago',
		'sex' => 'required|string',
		'phone' => 'phone:AUTO,mobile',
		'city' => 'string'
	];

	public function update(Request $request)
	{
		$user = $this->validateUserUpdate($request);

		$user->fill($request->all());

		$user->save();

		return new JsonResponse(['success' => 'User successfully updated'], 200);
	}

	public function validateUserUpdate(Request $request)
	{
		$validator = $this->getValidationFactory();

		// Validate the request
		$validator = $validator->make($request->all(), self::RULES_UPDATE_REQUEST);

		// Validate auth
		$user = $this->validateAuth($validator, $request->token);

		return $user;
	}

	public function validateAuth($validator, $token)
	{
		$decryptedToken = base64_decode($token);
		$tokenParts = explode(' ', $decryptedToken);

		// If we have no password return false
		if (empty($tokenParts[1])) {
			return false;
		}

		$username = $tokenParts[0];

		$user = User::query()
			->where([
				'username' => $username
			])->first();

		try {
			$password = decrypt($tokenParts[1]);
		} catch (\Exception $e) {
			// In case the decrypt payload is invalid
			$this->throwInvalidUserException($validator);
		}

		if (!$user || !Hash::check($password, $user->password)) {
			$this->throwInvalidUserException($validator);
		}

		return $user;
	}

	public function throwInvalidUserException($validator)
	{
		throw new ValidationException($validator, new JsonResponse(['auth' => 'User not found'], 422));
	}
}