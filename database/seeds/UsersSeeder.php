<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\User::query()
		    ->insert([
				[
					'username' => 'testuser',
			        'password' => app('hash')->make('testpass')
				],
				[
					'username' => 'testuser2',
			        'password' => app('hash')->make('testpass2')
				],
				[
					'username' => 'testuser3',
			        'password' => app('hash')->make('testpass3')
				],
	        ]);
    }
}
